﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class StatisticsLogger {
	
	public Dictionary<int,float> bestFitness;
	public Dictionary<int,float> meanFitness;
	public Dictionary<int,float> worstFitness;
	public Dictionary<int,float> deviationFitness;


	private string filename;

	private StreamWriter logger;

	public StatisticsLogger() {
		//filename = name;
		bestFitness = new Dictionary<int,float> ();
		meanFitness = new Dictionary<int,float> ();
		worstFitness = new Dictionary<int,float> ();
		deviationFitness = new Dictionary<int,float> ();

	}

	//saves fitness info and writes to console
	public void PostGenLog(List<Individual> pop, int currentGen, int counter ) {
		int generationSize = GameObject.FindObjectOfType<EvolutionState> ().numGenerations;

		pop.Sort((x, y) => -x.fitness.CompareTo(y.fitness));

		currentGen += counter * generationSize;

		bestFitness.Add (currentGen, pop[0].fitness);
		meanFitness.Add (currentGen, 0f);
		worstFitness.Add (currentGen, pop[pop.Count-1].fitness);
		deviationFitness.Add (currentGen, 0f);

		foreach (Individual ind in pop) {
			meanFitness[currentGen]+=ind.fitness;
		}
		meanFitness [currentGen] /= pop.Count;

		foreach (Individual ind in pop) {
			deviationFitness [currentGen] += Mathf.Abs (ind.fitness - meanFitness [currentGen]);
		}

		deviationFitness [currentGen] /= pop.Count;

		Debug.Log ("generation: "+(currentGen)+"\tbest: " + bestFitness [currentGen] + "\tmean: " + meanFitness [currentGen]+"\n");
	}

	//writes to file
	public void finalLog(string name) {
		float count = 0, mean = 0, best = 0, stddev = 0;
			
		int i;
		int generationSize = GameObject.FindObjectOfType<EvolutionState> ().numGenerations;

		this.filename = name;

		using (logger = new StreamWriter("all "+filename, false))
		{
			for (i = 0; i < bestFitness.Count; i++) 
				logger.WriteLine ("Generation: "+ i + "\t\tMean: " + meanFitness [i] + "\t\tBest: " + bestFitness [i] + "\t\tWorst: " + worstFitness [i] + "\t\tStandard Deviation: " + deviationFitness [i]);
			
		}
		logger.Close ();

		using (logger = new StreamWriter(filename, false))
		{
			for (i = generationSize - 1; i < bestFitness.Count; i += generationSize) {
				count++;
				mean += meanFitness [i];
				best += bestFitness [i];
				stddev += deviationFitness [i];
				logger.WriteLine ("Generation: " + i + "\t\tMean: " + meanFitness [i] + "\t\tBest: " + bestFitness [i] + "\t\tWorst: " + worstFitness [i] + "\t\tStandard Deviation: " + deviationFitness [i]);
			}
			logger.WriteLine ();
			logger.WriteLine ("Mean: " + mean/count + "\t\tBest: " + best/count + "\t\tStandard Deviation: " + stddev/count);

		}

		logger.Close ();

	}
}
