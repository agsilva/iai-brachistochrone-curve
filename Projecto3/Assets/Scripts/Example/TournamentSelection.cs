﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TournamentSelection : SelectionMethod {

	public TournamentSelection(): base() {

	}


	public override List<Individual> selectIndividuals (List<Individual> oldpop, int num)
	{
		return tournamentSelection (oldpop, num);
	}


	List<Individual> tournamentSelection(List<Individual> oldpop, int num) {

		List<Individual> selectedInds = new List<Individual> ();
		float percentage = GameObject.FindObjectOfType<EvolutionState>().tournamentPercentage;
		int tournamentSize = GameObject.FindObjectOfType<EvolutionState> ().tournamentValue;



		for (int i = 0; i<num; i++) {

			Individual ind = tournamentWinner(oldpop, percentage, tournamentSize);
			while (selectedInds.Contains(ind)) {
				ind = tournamentWinner(oldpop, percentage, tournamentSize);
			}
			selectedInds.Add (ind.Clone()); //we return copys of the selected individuals
		}

		return selectedInds;
	}

	Individual tournamentWinner(List<Individual> oldpop, float percentage, int tournamentSize)
	{
		
		int popsize;
		List<Individual> tournamentInds;
		tournamentInds = new List<Individual> ();
		popsize = oldpop.Count;

		//create the tournament
		for (int j = 0; j<tournamentSize; j++) {
			//make sure tournament individuals are different
			Individual ind = oldpop [Random.Range (0, popsize)];
			while (tournamentInds.Contains(ind)) {
				ind = oldpop [Random.Range (0, popsize)];
			}
			tournamentInds.Add (ind);
		}
		tournamentInds.Sort ((x, y) => -x.fitness.CompareTo (y.fitness));

		//find the tournament winner
		float r = Random.value;
		for (int j = 0; j < tournamentSize; j++)
		{
			if (r < percentage)
				return tournamentInds [j];
			r = (r - percentage) / (1 - percentage);
		}
		return tournamentInds [tournamentSize - 1];
	}

}
	
