﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RouletteSelection : SelectionMethod {

	public RouletteSelection(): base() {

	}


	public override List<Individual> selectIndividuals (List<Individual> oldpop, int num)
	{
		return rouletteSelection (oldpop, num);
	}


	List<Individual> rouletteSelection(List<Individual>oldpop, int num) {

		List<Individual> selectedInds = new List<Individual> ();
		int popsize = oldpop.Count;
		float totalfitness = 0;
		float currRange = 0;

		List<float> range = new List<float>();

		for (int i = 0; i < popsize; i++)
		{
			totalfitness += Mathf.Pow(oldpop [i].fitness,6);

		}
		for (int i = 0; i < popsize; i++)
		{
			currRange += Mathf.Pow(oldpop [i].fitness,6) / totalfitness;
			range.Add (currRange);

		}
		for (int i = 0; i<num; i++) {
			//make sure selected individuals are different
			float r = Random.value;
			Individual ind = null;
			for(int j = 0; j<popsize ; j++)
			{
				if (range [j] >= r)
				{
					ind = oldpop [j];
					break;
				}
			}
			if (ind == null)
				ind = oldpop [popsize - 1];
			while (selectedInds.Contains(ind)) {
				for(int j = 0; j<popsize ; j++)
				{
					if (range [j] >= r)
					{
						ind = oldpop [j];
						break;
					}
				}
				if (ind == null)
					ind = oldpop [popsize - 1];
			}
			selectedInds.Add (ind.Clone()); //we return copys of the selected individuals
		}

		return selectedInds;
	}

}
