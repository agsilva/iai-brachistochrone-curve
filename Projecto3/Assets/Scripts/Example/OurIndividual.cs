﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class OurIndividual : Individual {


	private float MinX;
	private float MaxX;
	private float MinY;
	private float MaxY;

	float distance; 

	public OurIndividual(ProblemInfo info) : base(info) {

		float dX;
		float dY;
		float d ;


		MinX = info.startPointX;
		MaxX = info.endPointX;
		MaxY = info.startPointY > info.endPointY ? info.startPointY : info.endPointY;
		MinY = MaxY - 2 * (Mathf.Abs (info.startPointY - info.endPointY));

		dX = MaxX-MinX;
		dY = info.endPointY-info.startPointY;
		//distance between points
		d = Mathf.Sqrt (dX*dX + dY*dY);

		MinY = MinY > d/Mathf.PI ? d/Mathf.PI : MinY;
		points = new List<Vector2> ();
		distance = d / (info.numTrackPoints * 2f);

	}

	public override void Initialize() {
		EnumType4 representation = (EnumType4) GameObject.FindObjectOfType<EvolutionState>().representation;
		if (representation == EnumType4.Dictionary)
			RandomInitialization ();
		else
			RandomInitialization2 ();
	}

	public override void Mutate(float probability) {
		EnumType4 representation = (EnumType4) GameObject.FindObjectOfType<EvolutionState>().representation;
		if (representation == EnumType4.Dictionary)
			NewValueMutation (probability);
		else
			NewValueMutation2 (probability);
		
	}
		

	public override void Crossover(Individual partner, float probability) {
		EnumType3 crossover = (EnumType3) GameObject.FindObjectOfType<EvolutionState>().CrossoverMethod;
		if (crossover == EnumType3.NPoints)
		{
			int nPoints = GameObject.FindObjectOfType<EvolutionState> ().nPoints;
			nPointCrossover (partner, probability, nPoints);
		}
		else
			HalfCrossover (partner, probability);
	}

	public override void CalcTrackPoints() {
		EnumType4 representation = (EnumType4) GameObject.FindObjectOfType<EvolutionState>().representation;
		if (representation == EnumType4.List) {
			for (int i = 0; i < points.Count; i++) {
				trackPoints.Add (points [i].x, points [i].y);
			}	
		}
	}

	public override void CalcFitness() {
		fitness = 1f/eval.time; //in this case we only consider time
	}


	public override Individual Clone() {
		OurIndividual newobj = (OurIndividual)this.MemberwiseClone ();
		newobj.fitness = 0f;
		newobj.trackPoints = new Dictionary<float,float> (this.trackPoints);
		return newobj;
	}



	void RandomInitialization() {
		float step = (info.endPointX - info.startPointX ) / (info.numTrackPoints - 1);
		float y = 0;

		trackPoints.Add (info.startPointX, info.startPointY);//startpoint
		for(int i = 1; i < info.numTrackPoints - 1; i++) {
			y = UnityEngine.Random.Range(MinY, MaxY);
			trackPoints.Add(info.startPointX + i * step, y);
		}
		trackPoints.Add (info.endPointX, info.endPointY); //endpoint
	}

	void RandomInitialization2() {
		Vector2 start = new Vector2 (info.startPointX, info.startPointY);
		Vector2 end = new Vector2 (info.endPointX, info.endPointY);
		Vector2 newPoint;
		points.Add (start);//startpoint
		points.Add (end); //endpoint
		bool inserting;

		for(int i = 1; i < info.numTrackPoints - 1; i++) {

			inserting = true;
			newPoint = new Vector2 (UnityEngine.Random.Range(MinX, MaxX), UnityEngine.Random.Range(MinY, MaxY));

			while (inserting) {
				if (canInsertPoint (newPoint)) {
					points.Add (newPoint);
					inserting = false;
				}
			}
		}
		points.Sort ((a, b) => a.x.CompareTo(b.x));

		for (int i = 0; i < info.numTrackPoints ; i++) {
			Debug.Log ("x= " + points [i].x + "   y= " + points [i].y);
		}

	}

	public override bool canInsertPoint(Vector2 newPoint) {
		 
		for (int i = 0; i < points.Count; i++) {
			if (points [i].x == newPoint.x)
				return false;
			else if ((points [i] - newPoint).magnitude <= distance)
				return false;
		}
		return true;
	}


	void NewValueMutation(float probability) {
		List<float> keys = new List<float>(trackPoints.Keys);
		float oldY, newY;

		//float gaussianMultiplier =  GameObject.FindObjectOfType<EvolutionState>().gaussianMultiplier;

		EnumType2 mutator = (EnumType2) GameObject.FindObjectOfType<EvolutionState>().Mutator;

		foreach (float x in keys) {
			//make sure that the startpoint and the endpoint are not mutated 
			if(Math.Abs (x-info.startPointX)<0.01 || Math.Abs (x-info.endPointX)<0.01) {
				continue;
			}

			if (mutator == EnumType2.Gaussian)
			{
				oldY = trackPoints [x];
				float r1 = (float)(UnityEngine.Random.value * 2.4 - 1.2);

				newY = oldY + Mathf.Pow (Mathf.Tan (r1), 3);
				if (newY > MaxY)
					newY = MaxY;
				if (newY < MinY)
					newY = MinY;
				trackPoints [x] = newY;
			}
			else
				trackPoints[x] = UnityEngine.Random.Range(MinY,MaxY);

		}
	}

	void NewValueMutation2 (float probability)
	{
		Vector2 newPoint, oldPoint, dif;
		bool mutating = true;

		EnumType2 mutator = (EnumType2) GameObject.FindObjectOfType<EvolutionState>().Mutator;


		for (int i = 1; i < points.Count - 1; i++) {
			oldPoint = points [i];
			points.RemoveAt(i);
			if(UnityEngine.Random.value < probability) {

				if (mutator == EnumType2.Gaussian) {
					while (mutating) {
						dif = new Vector2 ((float)(UnityEngine.Random.value * 2.4 - 1.2), (float)(UnityEngine.Random.value * 2.4 - 1.2));
						oldPoint += dif;
						if (oldPoint.y > MaxY)
							oldPoint.y = MaxY;
						if (oldPoint.y < MinY)
							oldPoint.y = MinY;
						if (oldPoint.x < MaxX && oldPoint.x > MinX) {
							if (canInsertPoint (oldPoint)) {
								points.Insert (i, oldPoint);
								mutating = false;
							}
						}						
					}
				} else {
					while (mutating) {
						newPoint = new Vector2 (UnityEngine.Random.Range (MinX, MaxX), UnityEngine.Random.Range (MinY, MaxY));
						if (canInsertPoint (newPoint)) {
							points.Insert (i, newPoint);
							mutating = false;
						}
					}
				}
			}
		}
		points.Sort ((a, b) => a.x.CompareTo(b.x));
	}

	void HalfCrossover(Individual partner, float probability) {

		if (UnityEngine.Random.Range (0f, 1f) > probability) {
			return;
		}
		//this example always splits the chromosome in half
		int crossoverPoint = Mathf.FloorToInt (info.numTrackPoints / 2f);
		List<float> keys = new List<float>(trackPoints.Keys);
		for (int i=0; i<crossoverPoint; i++) {
			float tmp = trackPoints[keys[i]];
			trackPoints[keys[i]] = partner.trackPoints[keys[i]];
			partner.trackPoints[keys[i]]=tmp;
		}

	}

	void HalfCrossover2(Individual partner, float probability) {


		if (UnityEngine.Random.Range (0f, 1f) > probability) {
			return;
		}
		//this example always splits the chromosome in half
		int crossoverPoint = Mathf.FloorToInt (info.numTrackPoints / 2f);

		for (int i=0; i<crossoverPoint; i++) {
			Vector2 tmp1 = points [i];
			Vector2 tmp2 = partner.points [i];
			points.RemoveAt (i);
			partner.points.RemoveAt (i);
			if (canInsertPoint (tmp2) && partner.canInsertPoint (tmp1)) {
				points.Insert(i,tmp2);
				partner.points.Insert(i,tmp1);
			}
		}
		points.Sort ((a, b) => a.x.CompareTo(b.x));
	}

	void nPointCrossover (Individual partner, float probability, int nPoints) {

		if (UnityEngine.Random.Range (0f, 1f) > probability) {
			return;
		}

		List<int> cutIndex = new List<int> ();

		int ntrackPoints = info.numTrackPoints;

		//para não demorar muito tempo
		if (nPoints > ntrackPoints / 2)
		{
			List<int> nocutIndex = new List<int> ();
			nPoints = ntrackPoints - nPoints;
			for (int i = 0; i < nPoints; i++)
			{
				int index = UnityEngine.Random.Range (0, ntrackPoints);
				while (nocutIndex.Contains (index))
					index = UnityEngine.Random.Range (0, ntrackPoints);
				nocutIndex.Add (index);
			}
			nPoints = ntrackPoints - nPoints;
			for (int i = 0; i < ntrackPoints; i++)
			{
				if(!nocutIndex.Contains (i))
					cutIndex.Add (i);
			}
		}
		else
		{
			for (int i = 0; i < nPoints; i++)
			{
				int index = UnityEngine.Random.Range (0, ntrackPoints);
				while (cutIndex.Contains (index))
					index = UnityEngine.Random.Range (0, ntrackPoints);
				cutIndex.Add (index);

			}

		}
		//adicionar um corte final
		if (nPoints % 2 == 1)
		{
			cutIndex.Add (ntrackPoints);
			nPoints++;
		}


		List<float> keys = new List<float>(trackPoints.Keys);
		/*string sp1 = "P1-> ";
		string sp2 = "P2-> ";
		string cut = "cut -> ";
		string sf1 = "F1-> ";
		string sf2 = "F2-> ";

		for (int i = 0; i < ntrackPoints; i++)
		{
			sp1 += trackPoints[keys[i]] + "  ";
			sp2 += partner.trackPoints[keys[i]] + "  ";
		}

		Debug.Log (sp1);
		Debug.Log (sp2);
*/


		for (int i = 0; i < nPoints ; i+=2)
		{
			/*cut += cutIndex [i] + "  " + cutIndex [i+1] + "  ";*/
			for (int j = cutIndex [i]; j < cutIndex [i + 1]; j++)
			{
				float tmp = trackPoints[keys[j]];
				trackPoints[keys[j]] = partner.trackPoints[keys[j]];
				partner.trackPoints[keys[j]]=tmp;
			}

		}
		/*for (int i = 0; i < ntrackPoints; i++)
		{
			sf1 += trackPoints[keys[i]] + "  ";
			sf2 += partner.trackPoints[keys[i]] + "  ";
		}
		Debug.Log (cut);
		Debug.Log (sf1);
		Debug.Log (sf2);*/

	}

	void nPointCrossover2 (Individual partner, float probability, int nPoints) {

		if (UnityEngine.Random.Range (0f, 1f) > probability) {
			return;
		}

		List<int> cutIndex = new List<int> ();

		int ntrackPoints = info.numTrackPoints;

		//para não demorar muito tempo
		if (nPoints > ntrackPoints / 2)
		{
			List<int> nocutIndex = new List<int> ();
			nPoints = ntrackPoints - nPoints;
			for (int i = 0; i < nPoints; i++)
			{
				int index = UnityEngine.Random.Range (0, ntrackPoints);
				while (nocutIndex.Contains (index))
					index = UnityEngine.Random.Range (0, ntrackPoints);
				nocutIndex.Add (index);
			}
			nPoints = ntrackPoints - nPoints;
			for (int i = 0; i < ntrackPoints; i++)
			{
				if(!nocutIndex.Contains (i))
					cutIndex.Add (i);
			}
		}
		else
		{
			for (int i = 0; i < nPoints; i++)
			{
				int index = UnityEngine.Random.Range (0, ntrackPoints);
				while (cutIndex.Contains (index))
					index = UnityEngine.Random.Range (0, ntrackPoints);
				cutIndex.Add (index);

			}

		}
		//adicionar um corte final
		if (nPoints % 2 == 1)
		{
			cutIndex.Add (ntrackPoints);
			nPoints++;
		}
			

		for (int i = 0; i < nPoints ; i+=2)
		{
			/*cut += cutIndex [i] + "  " + cutIndex [i+1] + "  ";*/
			for (int j = cutIndex [i]; j < cutIndex [i + 1]; j++)
			{
				Vector2 tmp1 = points [j];
				Vector2 tmp2 = partner.points [j];
				points.RemoveAt (j);
				partner.points.RemoveAt (j);
				if (canInsertPoint (tmp2) && partner.canInsertPoint (tmp1)) {
					points.Insert(j,tmp2);
					partner.points.Insert(j,tmp1);
				}
			}

		}

	}


	public enum EnumType2 { Random , Gaussian }
	public enum EnumType3 { NPoints, Half }
	public enum EnumType4 { Dictionary, List }

}
