﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EvolutionState : MonoBehaviour {

	public float startPointX;
	public float startPointY;
	public float endPointX;
	public float endPointY;
	public float g;
	public float startVelocity;
	public int numTrackPoints;
	private ProblemInfo info;

	public EnumType4 representation;

	public EnumType selectionMethod;
	//public bool random, roulette, tournament;//talvez +
	public int tournamentValue = 0;	
	public float tournamentPercentage = 1.0f;


	public int numGenerations;
	public int populationSize;

	public EnumType2 Mutator;
	public float gaussianMultiplier = 1f;
	public float mutationProbability;

	public EnumType3 CrossoverMethod;
	public int nPoints;
	public float crossoverProbability;


	private List<Individual> population;
	private SelectionMethod selectionMethod1;

	private int evaluatedIndividuals;
	private int currentGeneration;
	public int EvaluationsPerStep;

	public int elistimNumber;

	private StatisticsLogger stats;
	public string statsFilename;

	private PolygonGenerator drawer;
	public int repetitions;
	public int counter = 0;
	bool evolving;
	bool drawing;

	private string ficheiro;

	// Use this for initialization
	void Start () {
		info = new ProblemInfo ();
		info.startPointX = startPointX;
		info.startPointY = startPointY;
		info.endPointX = endPointX;
		info.endPointY = endPointY;
		info.g = g;
		info.startVelocity = startVelocity;
		info.numTrackPoints = numTrackPoints;
		currentGeneration = 0;
		// Verifies if the start and end points given by the user are valid
		if (startPointY < endPointY){

			Debug.LogWarning ("Start point cannot be lower than end point!");
			return;
		}
			
		if (selectionMethod == EnumType.Random)
		{
			selectionMethod1 = new RandomSelection (); //change accordingly
			tournamentValue = 0;
		}
		else if (selectionMethod == EnumType.Roulette)
		{
			selectionMethod1 = new RouletteSelection ();
			tournamentValue = 0;
		} 
		else if (selectionMethod == EnumType.Tournament)
		{
			selectionMethod1 = new TournamentSelection ();
			if (tournamentValue <= 0 || tournamentValue >= numTrackPoints || (tournamentPercentage > 1 || tournamentPercentage < 0))
			{
				Debug.LogWarning ("Check Tournamente variables");
				return;
			}
		}


		if (CrossoverMethod == EnumType3.NPoints) {
			if (nPoints == 0 || nPoints > numTrackPoints) {
				Debug.LogWarning ("Choose better nPoints!");
				return;
			}
		} else
			nPoints = 0;


		if (elistimNumber < 0 || elistimNumber >= populationSize)
		{
			Debug.LogWarning ("Check elitismNumber variables");
			return;
		}

		if (counter == 0) {
			stats = new StatisticsLogger ();
			ficheiro = statsFilename;
		}

		drawer = new PolygonGenerator ();	
		InitPopulation ();
		evaluatedIndividuals = 0;
		currentGeneration = 0;
		evolving = true;
		drawing = false;

	}
	

	void FixedUpdate () {
		if (evolving) {
			EvolStep ();
		} else if(drawing) {
			population.Sort((x, y) => -x.fitness.CompareTo(y.fitness));
			drawer.drawCurve(population[0].trackPoints,info);
			drawing=false;
		}
	}

	void EvolStep() {
		//do for a given number of generations
		if (currentGeneration < numGenerations) {
			
			//if there are individuals to evaluate on the current generation
			int evalsThisStep = EvaluationsPerStep < (populationSize - evaluatedIndividuals) ? EvaluationsPerStep : (populationSize - evaluatedIndividuals);
			for (int ind = evaluatedIndividuals; ind<evaluatedIndividuals+evalsThisStep; ind++) {
				population[ind].evaluate();
			}
			evaluatedIndividuals += evalsThisStep;
			
			//if all individuals have been evaluated on the current generation, breed a new population
			if(evaluatedIndividuals==populationSize) {
				stats.PostGenLog(population,currentGeneration, counter);
				
				population = BreedPopulation();
				evaluatedIndividuals=0;
				currentGeneration++;
			}
			
		} else {

			if (counter < repetitions-1) {
				counter++;
				Start ();
			} else {
				stats.finalLog (ficheiro);

				/*if (nextCombo ()) {
					
					ficheiro ="BY_"+ endPointY+ " tPoints_"+ numTrackPoints+" "+ selectionMethod.ToString()+tournamentValue+" PopSize_"+ populationSize+" Mutator_"
						+ Mutator.ToString()+ " MutProb_"+ mutationProbability.ToString()+" Crossover_" + CrossoverMethod.ToString ()+nPoints+" CrossProb_"+ crossoverProbability.ToString()+".txt";

					stats = new StatisticsLogger ();
					Start ();
				}
			
				else {*/
					evolving = false;
					drawing = true;
					print ("evolution stopped");
				//}
			}

		}
	
	}
	//v1 b510 b010
	bool nextCombo()
	{
		counter = 0;

		if (endPointY == 0) {
			endPointY = 5;
			return true;
		}

		endPointY = 0;

		return nextCombo2 ();
	}

	//v2 tPoints 20 50
	bool nextCombo2()
	{

		if (numTrackPoints == 20) {
			numTrackPoints = 50;

			return true;
		}

		numTrackPoints = 20;

		return nextCombo3 ();
	}

	//v3 Selection Random Roulette Torneio3 Torneio5
	bool nextCombo3()
	{

		/*if (selectionMethod == EnumType.Random) {
			selectionMethod = EnumType.Roulette;

			return true;
		}


		if (selectionMethod == EnumType.Roulette) {
			selectionMethod = EnumType.Tournament;
			tournamentValue = 7;


			return true;
		}*/


		if (tournamentValue == 13) {
			tournamentValue = 15;
			return true;
		}

		if (tournamentValue == 15) {
			tournamentValue = 17;
			return true;
		}

		/*selectionMethod = EnumType.Random;
		tournamentValue = 0;*/
		tournamentValue = 13;

		return nextCombo4 ();
	}

	//v4 popSize 100 1000
	bool nextCombo4()
	{

		if (populationSize == 100) {
			populationSize = 1000;
			elistimNumber = 10;

			return true;
		}

		populationSize = 100;
		elistimNumber = 1;

		return nextCombo5 ();
	}

	//v5 mutator Random Gaussian
	bool nextCombo5()
	{

		if (Mutator == EnumType2.Random) {
			Mutator = EnumType2.Gaussian;
			return true;
		}

		Mutator = EnumType2.Random;

		return nextCombo6 ();
	}

	//v6 mutation probability
	bool nextCombo6()
	{

		if (mutationProbability == 0.1f) {
			mutationProbability = 0.05f;
			return true;
		}

		if (mutationProbability == 0.05f) {
			mutationProbability = 0.01f;
			return true;
		}

		if (mutationProbability == 0.01f) {
			mutationProbability = 0.5f;
			return true;
		}

		mutationProbability = 0.1f;

		return nextCombo7 ();
	}

	//v7 crossover Half NPoints
	bool nextCombo7()
	{

		if (CrossoverMethod == EnumType3.Half) {
			CrossoverMethod = EnumType3.NPoints;

			nPoints = 3;
			return true;
		}

		if (nPoints == 3) {
			nPoints = 10;
			return true;
		}

		CrossoverMethod = EnumType3.Half;		
		nPoints = 0;
		return nextCombo8();
	}

	//v8 crossover probability
	bool nextCombo8()
	{
		if (crossoverProbability == 0.9f) {
			crossoverProbability = 0.95f;
			return true;
		}
		if (crossoverProbability == 0.95f) {
			crossoverProbability = 1;
			return true;
		}

		crossoverProbability = 0.95f;

		return false;
	}

	void InitPopulation () {
		population = new List<Individual>();
		while (population.Count<populationSize) {
			OurIndividual newind = new OurIndividual(info); //change accordingly
			newind.Initialize();
			population.Add (newind);
		}
	}

	List<Individual> BreedPopulation() {
		List<Individual> newpop = new List<Individual> ();

		//ordenar em ordem crescente de fitness
		population.Sort((x, y) => -x.fitness.CompareTo(y.fitness));

		//elitism
		for (int i = 0; i < elistimNumber; i++)
		{
			newpop.Add (population [i].Clone());
		}

		List<Individual> selectedInds;


		//breed individuals and place them on new population. We'll apply crossover and mutation later 
		while(newpop.Count<populationSize) {
			selectedInds = selectionMethod1.selectIndividuals(population,2); //we should propably always select pairs of individuals



			for(int i =0; i< selectedInds.Count;i++) {
				if(newpop.Count<populationSize) {
					newpop.Add(selectedInds[i]); //added individuals are already copies, so we can apply crossover and mutation directly
				}
				else{ //discard any excess individuals
					selectedInds.RemoveAt(i);	
				}
			}

			//apply crossover between pairs of individuals and mutation to each one
			while(selectedInds.Count>1) {
				selectedInds[0].Crossover(selectedInds[1],crossoverProbability);
				selectedInds[0].Mutate(mutationProbability);
				selectedInds[1].Mutate(mutationProbability);
				selectedInds.RemoveRange(0,2);
			}
			if(selectedInds.Count==1) {
				selectedInds[0].Mutate(mutationProbability);
				selectedInds.RemoveAt(0);
			}
		}

		return newpop;
	}
	public enum EnumType { Random , Roulette , Tournament }

	public enum EnumType2 { Random , Gaussian }

	public enum EnumType3 { NPoints, Half }

	public enum EnumType4 { Dictionary, List }

}


