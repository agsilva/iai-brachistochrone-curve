﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct ProblemInfo {
	public float startPointX;
	public float startPointY;
	public float endPointX;
	public float endPointY;
	
	public float g;
	public float startVelocity;
	
	public int numTrackPoints;
}

public struct FitnessInfo {
	public float time;
	public float pointsCleared;
	public float distanceTraveled;
}


public class BrachystochroneProblem {

	public ProblemInfo info ;

	public BrachystochroneProblem(ProblemInfo inf){
		info = inf;
	}


	public FitnessInfo evaluate(Dictionary<float,float>trackpoints) {

		FitnessInfo eval = new FitnessInfo ();
		eval.time = 0;
		eval.pointsCleared = 0;
		//float ParTime = 0;

		List<float> pointsX = new List<float> (trackpoints.Keys);
		pointsX.Sort ();

		float pointA = pointsX [0];
		float vi = info.startVelocity;

		float pointB;

		float dX;
		float dY;
		float d ;
		float vfsq ;
		float vf ;
		float vm ;
		float t;

		for (int i=1;i<pointsX.Count;i++) {

			pointB = pointsX[i];

			dX = pointB-pointA;
			dY = trackpoints[pointB]-trackpoints[pointA];
			//distance between points
			d = Mathf.Sqrt (dX*dX + dY*dY);

			//squared final velocity
			vfsq = vi * vi + 2 * info.g * dY;
			//final velocity
			vf = Mathf.Sqrt (vfsq);
			//medium velocity
			vm = (vi + vf) / 2;
			//time taken
			t = d / vm;

			vi = vf;
			if(vfsq<0) {
				eval.time=Mathf.Infinity;
				return eval;
			}

			pointA=pointB;
			eval.time+=t;
			eval.pointsCleared++;
			eval.distanceTraveled+=d;
		}

		return eval;
	}
}
