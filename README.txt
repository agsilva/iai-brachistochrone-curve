Para correr o projecto:
- Abrir a pasta 'Projecto3' atraves do Unity.

Caso ocorra algum problema:
- Criar novo projecto Unity e importar o Projecto3.unitypackage

O projecto permite alterar os parametros indicados no Inspector (devidamente protegidos) e cria ainda 2 ficheiros por cada experiencia:
- all 'filename': contem todas as gera�oes.
- 'filename': contem apenas a melhor gera�ao de cada repeti�ao e a media das mesmas.

Nota: Todas as experencias efectuadas estao em ficheiros TXT nas pastas "All Results" e "Best Results". Para melhor visualiza�ao das mesmas, basta abrir o ficheiro "Resultados- Folha1 e 2.xlsx"
